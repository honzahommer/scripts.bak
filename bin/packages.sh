#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

if [ $EUID != 0 ]
then
    echo "ERROR: You have to be root to execute this script"
    exit 1
fi

apt-get -y install apt-transport-https ca-certificates curl git htop ntpdate sudo

} # this ensures the entire script is downloaded #
