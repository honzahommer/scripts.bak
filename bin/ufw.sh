#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

if [ $EUID != 0 ]
then
    echo "ERROR: You have to be root to execute this script"
    exit 1
fi

UFW_RESET=false

for i in "$@"
do
    case $i in
        -r=*|--reset=*)
        UFW_RESET=true
        ;;
    esac
done

if [[ ! -x `command -v ufw` ]]
then
    apt-get -y install ufw
elif [[ $UFW_RESET = "true" ]]
then
    ufw -y reset
fi

ufw default deny incoming
ufw default allow outgoing

ufw allow ssh
ufw allow http
ufw allow https

ufw --force enable

unset -f UFW_RESET

} # this ensures the entire script is downloaded #
