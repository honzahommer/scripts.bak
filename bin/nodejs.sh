#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

if [ $EUID != 0 ]
then
    echo "ERROR: You have to be root to execute this script"
    exit 1
fi

NPM_VERSION=""
NVM_VERSION=0.32.1
WITHOUT_PM2=false

for i in "$@"
do
case $i in
    --nvm=*)
    NVM_VERSION="${i#*=}"
    ;;
    --npm=*)
    NPM_VERSION="${i#*=}"
    ;;
    --no-pm2=*)
    WITHOUT_PM2=true
    ;;
esac
done

if [ ! -d $HOME/.nvm ]
then
    wget -qO- https://raw.githubusercontent.com/creationix/nvm/v$NVM_VERSION/install.sh | bash
fi

for DOTFILE in \
    $HOME/.nvm/nvm.sh \
    $HOME/.profile \
    $HOME/.bashrc
do
    if [ -f "$DOTFILE" ]
    then
        . $DOTFILE
    fi
done

if [[ $NPM_VERSION = "" ]]
then
    NPM_VERSION="`nvm version-remote`"
fi

nvm install $NPM_VERSION

if [[ $WITHOUT_PM2 = "false" ]]
then
    npm -g install pm2

    pm2 startup `lsb_release -is`
    pm2 web
    pm2 update
fi

unset -f NVM_VERSION NPM_VERSION WITHOUT_PM2

} # this ensures the entire script is downloaded #
