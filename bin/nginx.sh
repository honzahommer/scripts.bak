#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

if [ $EUID != 0 ]
then
    echo "ERROR: You have to be root to execute this script"
    exit 1
fi

CERTS_BITS=4096
CERTS_PATH=/etc/ssl/certs
NGINX_AMPLIFY=""
NGINX_PATH=/etc/nginx
OS_ID="`lsb_release -is`"
OS_CODENAME="`lsb_release -cs`"
TIMESTAMP="`date +%Y%m%d%H%M%S`"

for i in "$@"
do
    case $i in
        -a=*|--amplify=*)
        NGINX_AMPLIFY="${i#*=}"
        ;;
        -b=*|--certsbits=*)
        CERTS_BITS="${i#*=}"
        ;;
        -c=*|--certspath=*)
        CERTS_PATH="${i#*=}"
        ;;
        -n=*|--nginxpath=*)
        NGINX_PATH="${i#*=}"
        ;;
    esac
done

if [ ! -d "$CERTS_PATH" ]
then
    mkdir -p $CERTS_PATH
fi

#if [ ! -f "$CERTS_PATH/dhparam.pem" ]
#then
#    openssl dhparam -out $CERTS_PATH/dhparam.pem $CERTS_BITS
#fi

if [ ! -f "$CERTS_PATH/selfsigned.crt" ] || [ ! -f "$CERTS_PATH/selfsigned.key" ]
then
    openssl req -x509 -nodes -days 365 -newkey rsa:$CERTS_BITS -keyout $CERTS_PATH/selfsigned.key -out $CERTS_PATH/selfsigned.crt -subj "/CN=`hostname -f`"
fi

if [[ ! -x `command -v nginx` ]]
then
    if [ ! -f "/etc/apt/sources.list.d/dotdeb.list" ]
    then
        echo "deb http://packages.dotdeb.org $OS_CODENAME all" | tee -a /etc/apt/sources.list.d/dotdeb.list
        echo "deb-src http://packages.dotdeb.org $OS_CODENAME all" | tee -a /etc/apt/sources.list.d/dotdeb.list
        wget -qO - http://www.dotdeb.org/dotdeb.gpg | apt-key add -

        apt-get -y update
    fi

    if [ $OS_ID = "Debian" ]
    then
        apt-get -y install nginx nginx-extras
    else
        apt-get -y install nginx
    fi
fi

if [ ! -d "$NGINX_PATH" ]
then
    mkdir -p $CERTS_PATH;
elif [ ! -d "$NGINX_PATH/.bak" ]
then
    mv $NGINX_PATH /tmp/nginx.$TIMESTAMP
    mkdir -p $NGINX_PATH
    mv /tmp/nginx.$TIMESTAMP $NGINX_PATH.bak-$TIMESTAMP
fi

cat <<EOT > $NGINX_PATH/nginx.conf
user nginx;
pid /run/nginx.pid;

worker_processes auto;

events {
    worker_connections 1024;
}

http {
    log_format main_ext '\$remote_addr - \$remote_user [\$time_local] "\$request" '
        '\$status \$body_bytes_sent "\$http_referer" '
        '"\$http_user_agent" "\$http_x_forwarded_for" '
        '"\$host" sn="\$host" '
        'rt=\$request_time '
        'ua="\$upstream_addr" us="\$upstream_status" '
        'ut="\$upstream_response_time" ul="\$upstream_response_length" '
        'cs=\$upstream_cache_status' ;

    access_log /var/log/nginx/access.log main_ext;
    error_log /var/log/nginx/error.log warn;

    server_tokens off;
    more_set_headers 'Server: ';

    gzip on;
    gzip_disable "msie6";

    proxy_redirect off;
    proxy_http_version 1.1;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header X-Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$remote_addr;
    proxy_set_header X-Forwarded-Protocol \$scheme;

    ssl_certificate /etc/ssl/certs/selfsigned.crt;
    ssl_certificate_key /etc/ssl/certs/selfsigned.key;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    #ssl_dhparam /etc/ssl/certs/dhparam.pem;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';
    ssl_session_timeout 1d;
    ssl_session_cache shared:SSL:50m;

    default_type 'text/html';
    types {
        text/html html htm shtml; text/css css; text/xml xml; text/mathml mml; text/plain txt; text/vnd.sun.j2me.app-descriptor jad; text/vnd.wap.wml wml; text/x-component htc;
        image/gif gif; image/jpeg jpeg jpg; image/png png; image/tiff tif tiff; image/vnd.wap.wbmp wbmp; image/x-icon ico; image/x-jng jng; image/x-ms-bmp bmp; image/svg+xml svg svgz; image/webp webp;
        application/javascript js; application/atom+xml atom; application/rss+xml rss; application/font-woff woff; application/java-archive jar war ear; application/json json; application/mac-binhex40 hqx; application/msword doc; application/pdf pdf; application/postscript ps eps ai; application/rtf rtf; application/vnd.apple.mpegurl m3u8; application/vnd.ms-excel xls; application/vnd.ms-fontobject eot; application/vnd.ms-powerpoint ppt; application/vnd.wap.wmlc wmlc; application/vnd.google-earth.kml+xml kml; application/vnd.google-earth.kmz kmz; application/x-7z-compressed 7z; application/x-cocoa cco; application/x-java-archive-diff jardiff; application/x-java-jnlp-file jnlp; application/x-makeself run; application/x-perl pl pm; application/x-pilot prc pdb; application/x-rar-compressed rar; application/x-redhat-package-manager rpm; application/x-sea sea; application/x-shockwave-flash swf; application/x-stuffit sit; application/x-tcl tcl tk; application/x-x509-ca-cert der pem crt; application/x-xpinstall xpi; application/xhtml+xml xhtml; application/xspf+xml xspf; application/zip zip; application/octet-stream bin exe dll; application/octet-stream deb; application/octet-stream dmg; application/octet-stream iso img; application/octet-stream msi msp msm; application/vnd.openxmlformats-officedocument.wordprocessingml.document docx; application/vnd.openxmlformats-officedocument.spreadsheetml.sheet xlsx; application/vnd.openxmlformats-officedocument.presentationml.presentation pptx;
        audio/midi mid midi kar; audio/mpeg mp3; audio/ogg ogg; audio/x-m4a m4a; audio/x-realaudio ra;
        video/3gpp 3gpp 3gp; video/mp2t ts; video/mp4 mp4; video/mpeg mpeg mpg; video/quicktime mov; video/webm webm; video/x-flv flv; video/x-m4v m4v; video/x-mng mng; video/x-ms-asf asx asf; video/x-ms-wmv wmv; video/x-msvideo avi;
    }
    
    server {
        listen 80;

        server_name 127.0.0.1;

        location /nginx_status {
            stub_status on;

            allow 127.0.0.1;
            deny all;
        }
    }

    server {
        listen 80;
        listen 443 ssl;

        server_name ~^((?<proxy_pass_port>[0-9]*)\.)?(.*)\$;

        if (\$host = \$server_addr) {
            return 307 https://\$hostname\$request_uri;
        }

        if (\$scheme = "http") {
            return 307 https://\$host\$request_uri;
        }
        
        if (\$host = \$hostname) {
            set \$proxy_pass_port "";
        }

        error_page 502 503 504 =503 /@maintenance;

        location /@welcomepage {
            set \$code 200;
            set \$color "#4cae4c";
            set \$title "It works!";

            if (\$arg_error != "") {
                set \$code  503;
                set \$color "#e46";
                set \$title "Website is currently under maintenance and will be back shortly!";
            }

            echo_after_body '<!-- ::{ "code": \$code, "date": "\$date_gmt", "time": "\$echo_timer_elapsed", "host": "\$host", "node": "\$hostname", "path": "\$request_uri" }:: -->';
            echo '<!DOCTYPE html><title>\$title</title><style>body,html{height:100%}body{height:100%;width:100%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;margin:0}</style><body><svg height=70px version=1.1 viewBox="0 0 100 68"width=100.001px x=0px xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink y=0px><g id=large><g><path d=M55.777,38.473l6.221-1.133c0.017-1.791-0.123-3.573-0.41-5.324l-6.321-0.19c-0.438-2.053-1.135-4.048-2.076-5.931l4.82-4.094c-0.868-1.552-1.874-3.028-3.005-4.417l-5.569,2.999c-1.385-1.54-2.98-2.921-4.771-4.099l2.124-5.954c-0.759-0.452-1.543-0.878-2.357-1.269c-0.811-0.39-1.625-0.732-2.449-1.046l-3.325,5.381c-2.038-0.665-4.113-1.052-6.183-1.174L31.34,6.002c-1.792-0.02-3.571,0.119-5.32,0.406l-0.191,6.32c-2.056,0.439-4.051,1.137-5.936,2.08l-4.097-4.82c-1.546,0.872-3.022,1.875-4.407,3.006l2.996,5.566c-1.54,1.384-2.925,2.985-4.104,4.778c-2.16-0.771-4.196-1.498-5.953-2.127c-0.449,0.765-0.875,1.544-1.265,2.354c-0.39,0.811-0.733,1.63-1.049,2.457c1.587,0.981,3.424,2.119,5.377,3.325c-0.662,2.037-1.049,4.117-1.172,6.186l-6.218,1.136c-0.021,1.789,0.12,3.566,0.407,5.321l6.32,0.188c0.442,2.06,1.143,4.057,2.082,5.937l-4.818,4.095c0.872,1.549,1.873,3.026,3.009,4.412l5.563-2.998c1.392,1.54,2.989,2.92,4.777,4.099l-2.121,5.954c0.756,0.446,1.538,0.871,2.348,1.258c0.813,0.394,1.633,0.739,2.462,1.05l3.326-5.375c2.033,0.662,4.109,1.05,6.175,1.17l1.137,6.221c1.791,0.019,3.569-0.123,5.323-0.407l0.194-6.324c2.053-0.438,4.045-1.136,5.927-2.079l4.093,4.817c1.55-0.865,3.026-1.87,4.414-2.999l-2.995-5.572c1.537-1.385,2.914-2.98,4.093-4.772l5.953,2.127c0.448-0.761,0.878-1.545,1.268-2.356c0.388-0.808,0.729-1.631,1.047-2.458l-5.378-3.324C55.268,42.615,55.655,40.542,55.777,38.473zM42.302,42.435c-3.002,6.243-10.495,8.872-16.737,5.866c-6.244-2.999-8.872-10.493-5.867-16.736c3.002-6.244,10.495-8.873,16.736-5.869C42.676,28.698,45.306,36.19,42.302,42.435z fill=none stroke=\$color><animateTransform attributeName=transform begin=0s dur=9s from="0 31 37"repeatCount=indefinite to="360 31 37"type=rotate></animateTransform></g><g id=small><path d=M93.068,19.253L99,16.31c-0.371-1.651-0.934-3.257-1.679-4.776l-6.472,1.404c-0.902-1.436-2.051-2.735-3.42-3.819l2.115-6.273c-0.706-0.448-1.443-0.867-2.213-1.238c-0.774-0.371-1.559-0.685-2.351-0.958l-3.584,5.567c-1.701-0.39-3.432-0.479-5.118-0.284L73.335,0c-1.652,0.367-3.256,0.931-4.776,1.672l1.404,6.47c-1.439,0.899-2.744,2.047-3.835,3.419c-2.208-0.746-4.38-1.476-6.273-2.114c-0.451,0.71-0.874,1.448-1.244,2.229c-0.371,0.764-0.68,1.541-0.954,2.329c1.681,1.078,3.612,2.323,5.569,3.579c-0.399,1.711-0.486,3.449-0.291,5.145c-2.086,1.034-4.143,2.055-5.936,2.945c0.368,1.648,0.929,3.25,1.67,4.769c1.954-0.426,4.193-0.912,6.468-1.405c0.906,1.449,2.06,2.758,3.442,3.853l-2.117,6.27c0.708,0.449,1.439,0.865,2.218,1.236c0.767,0.371,1.551,0.685,2.338,0.96c1.081-1.68,2.319-3.612,3.583-5.574c1.714,0.401,3.457,0.484,5.156,0.288L82.695,42c1.651-0.371,3.252-0.931,4.773-1.676c-0.425-1.952-0.912-4.194-1.404-6.473c1.439-0.902,2.744-2.057,3.835-3.436l6.273,2.11c0.444-0.7,0.856-1.43,1.225-2.197c0.372-0.777,0.691-1.569,0.963-2.361l-5.568-3.586C93.181,22.677,93.269,20.939,93.068,19.253zM84.365,24.062c-1.693,3.513-5.908,4.991-9.418,3.302c-3.513-1.689-4.99-5.906-3.301-9.419c1.688-3.513,5.906-4.991,9.417-3.302C84.573,16.331,86.05,20.549,84.365,24.062z fill=none stroke=\$color><animateTransform attributeName=transform begin=0s dur=6s from="0 78 21"repeatCount=indefinite to="-360 78 21"type=rotate></animateTransform></g></svg>';
        }

        location /@maintenance {
            add_header Retry-After 500;
            echo_location_async /@welcomepage 'error=true';
        }

        location = /favicon.ico {
            log_not_found off;
            access_log off;
            try_files \$uri =204;
        }

        location / {
            echo_reset_timer;

            if (\$proxy_pass_port = "") {
                echo_location_async /@welcomepage;
            }

            proxy_pass http://127.0.0.1:\$proxy_pass_port;
        }
    }
}
EOT

useradd nginx
adduser nginx www-data

service nginx restart

if [[ $NGINX_AMPLIFY != "" ]]
then
    NGINX_AMPLIFY_SCRIPT=/tmp/$amplify.TIMESTAMP.sh
    
    wget -O $NGINX_AMPLIFY_SCRIPT https://github.com/nginxinc/nginx-amplify-agent/raw/master/packages/install.sh
    API_KEY=$NGINX_AMPLIFY sh $NGINX_AMPLIFY_SCRIPT
    
    rm -f $NGINX_AMPLIFY_SCRIPT
fi

unset -f API_KEY CERTS_BITS CERTS_PATH NGINX_AMPLIFY NGINX_AMPLIFY_SCRIPT NGINX_PATH OS_ID OS_CODENAME TIMESTAMP

} # this ensures the entire script is downloaded #
