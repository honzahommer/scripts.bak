#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

if [ $EUID != 0 ]
then
    echo "ERROR: You have to be root to execute this script"
    exit 1
fi

USERIFY_API_KEY=""

for i in "$@"
do
    case $i in
        --userify=*)
        USERIFY_API_KEY="${i#*=}"
        ;;
    esac
done

if [[ $USERIFY_API_KEY != "" ]]
then
    wget -qO- https://dashboard.userify.com/api/userify/install/id/$USERIFY_API_KEY | sh
    clear
fi

if [ -d "$HOME/.ssh" ] && [ -f "$HOME/.ssh/authorized_keys" ]
then
    sed -i -e "s/^PasswordAuthentication.*/PasswordAuthentication no/g" /etc/ssh/sshd_config
    sed -i -e "s/^PermitRootLogin.*/PermitRootLogin without-password/g" /etc/ssh/sshd_config
fi

unset -f USERIFY_API_KEY

} # this ensures the entire script is downloaded #