#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

if [ $EUID != 0 ]
then
    echo "ERROR: You have to be root to execute this script"
    exit 1
fi

apt-get -q -y update
apt-get -q -y dist-upgrade
apt-get -y autoremove

} # this ensures the entire script is downloaded #
