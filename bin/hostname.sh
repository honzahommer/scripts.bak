#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

if [ $EUID != 0 ]
then
    echo "ERROR: You have to be root to execute this script"
    exit 1
fi

HOSTNAME_DOM=.xip.io
HOSTNAME_OLD=`hostname`
HOSTNAME_NEW=""

for i in "$@"
do
    case $i in
        -h=*|--hostname=*)
        HOSTNAME_NEW="${i#*=}"
        ;;
        --domain=*)
        HOSTNAME_DOM=".${i#*=}"
        ;;
        --cf-key=*)
        CF_KEY=".${i#*=}"
        ;;
        --cf-email=*)
        CF_EMAIL=".${i#*=}"
        ;;
        --cf-force=*)
        CF_EMAIL=true
        ;;

    esac
done

if [[ $HOSTNAME_NEW = "" ]]
then
    if [[ $1 != "" ]] && [[ $HOSTNAME_DOM = "" ]]
    then
        HOSTNAME_NEW=$1
    fi

    HOSTNAME_NEW=`curl api.ipify.org`
    #HOSTNAME_NEW=`curl api.ipify.org | sed 's/\./-/g' | sed 's/\:/-/g'`
fi

HOSTNAME_NEW=$HOSTNAME_NEW$HOSTNAME_DOM

if [ $HOSTNAME_NEW != $HOSTNAME_OLD ]
then
    hostname $HOSTNAME_NEW
    
    for HOSTNAME_FILE in \
       /etc/exim4/update-exim4.conf.conf \
       /etc/printcap \
       /etc/hostname \
       /etc/hosts \
       /etc/ssh/ssh_host_rsa_key.pub \
       /etc/ssh/ssh_host_dsa_key.pub \
       /etc/motd \
       /etc/ssmtp/ssmtp.conf
    do
        if [ -f $HOSTNAME_FILE ]
        then
            sed -i "s/$HOSTNAME_OLD/$HOSTNAME_NEW/g" $HOSTNAME_FILE
        fi
    done
fi

rm -f /etc/ssh/ssh_host_*
ssh-keygen -f /etc/ssh/ssh_host_rsa_key -t rsa -N ''
ssh-keygen -f /etc/ssh/ssh_host_dsa_key -t dsa -N ''
ssh-keygen -f /etc/ssh/ssh_host_ecdsa_key -t ecdsa -N ''
ssh-keygen -f /etc/ssh/ssh_host_ed25519_key -t ed25519 -N ''

unset -f HOSTNAME_DOM HOSTNAME_FILE HOSTNAME_NEW HOSTNAME_OLD

} # this ensures the entire script is downloaded #
