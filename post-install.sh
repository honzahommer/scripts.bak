#!/usr/bin/env bash

{ # this ensures the entire script is downloaded #

if [ $EUID != 0 ]
then
    echo "ERROR: You have to be root to execute this script"
    exit 1
fi

for SCRIPT_NAME in \
    update \
    hostname \
    packages \
    ssh \
    dotfiles \
    ufw \
    nodejs \
    nginx
do
    SCRIPT_ARGS=$(eval echo \${`echo $SCRIPT_NAME | awk '{print toupper($0)}'`_ARGS})
    wget -qO- https://gitlab.com/honzahommer/scripts/raw/master/bin/$SCRIPT_NAME.sh | bash /dev/stdin $SCRIPT_ARGS
done

unset -f SCRIPT_NAME SCRIPT_ARGS

} # this ensures the entire script is downloaded #
