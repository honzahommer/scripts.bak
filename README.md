# [Honza Hommer](https://gitlab.com/honzahommer) / scripts
> Collection of useful scripts.

## Installation

To run post install script, you can use the install script using Wget:

```sh
wget -qO- https://gitlab.com/honzahommer/scripts/raw/master/post-install.sh | bash
```

<sub>The script set default hostname, update system, install and configure some
packages (UFW, NGINX, NVM, NPM, PM2...).</sub>


## Included scripts

*See script source file for possible arguments.*

```sh
# update.sh
# Update OS and packages

wget -qO- https://gitlab.com/honzahommer/scripts/raw/master/bin/update.sh | bash
```

```sh
# hostname.sh
# Set correct hostname

wget -qO- https://gitlab.com/honzahommer/scripts/raw/master/bin/hostname.sh | bash
```

```sh
# packages.sh
# Install recommended packages (like cURL, Git...)

wget -qO- https://gitlab.com/honzahommer/scripts/raw/master/bin/packages.sh | bash
```

```sh
# ssh.sh
# SSH security tweaks

wget -qO- https://gitlab.com/honzahommer/scripts/raw/master/bin/ssh.sh | bash
```

```sh
# dotfiles.sh
# Customized dotfiles, aliases and functions

wget -qO- https://gitlab.com/honzahommer/scripts/raw/master/bin/dotfiles.sh | bash
```

```sh
# uwf.sh
# Install UncomplicatedFirewall and allow SSH, HTTP and HTTPS access

wget -qO- https://gitlab.com/honzahommer/scripts/raw/master/bin/uwf.sh | bash
```

```sh
# nodejs.sh
# Install Node Version Manager, latest version of NodeJS and PM2

wget -qO- https://gitlab.com/honzahommer/scripts/raw/master/bin/nodejs.sh | bash
```

```sh
# nginx.sh
# Install NGINX with base config and self signed certificates

wget -qO- https://gitlab.com/honzahommer/scripts/raw/master/bin/nginx.sh | bash
```
